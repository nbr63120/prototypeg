/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.event

import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.util.ArrayList
import java.util.EventListener

open class EventManager {
	private val listeners = ArrayList<EventListener>()

	open fun registerListener(listener: EventListener) {
		listeners.add(listener)
	}

	open fun unregisterListener(listener: EventListener) {
		listeners.remove(listener)
	}

	open fun call(event: Event) {
		listeners.forEach { listener ->
			for (method in listener.javaClass.methods) {
				if (method.parameterCount != 1) {
					continue
				}
				method.isAccessible = true
				if (method.parameterTypes[0].isInstance(event)) {
					try {
						method.invoke(listener, event)
					} catch (e: IllegalAccessException) {
						e.printStackTrace()
					} catch (e: InvocationTargetException) {
						e.printStackTrace()
					}

				}
			}
		}
	}
}
