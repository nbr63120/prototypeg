package com.gitlab.prototypeg.event

import com.gitlab.prototypeg.doll.Doll

open class DollGetEvent(open var doll : Doll) : Event.Cancellable.Base()