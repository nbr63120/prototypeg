package com.gitlab.prototypeg.event

import com.gitlab.prototypeg.network.response.FriendVisitResponse

open class FriendVisitEvent(open val friendVisitResponse: FriendVisitResponse) : Event()