/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.request

import com.gitlab.prototypeg.gfencryption.Sign

import java.io.IOException
import java.io.UnsupportedEncodingException
import java.util.HashMap
import java.util.regex.Pattern

abstract class ParamRequest(buffer: ByteArray, path: String) : Request(buffer, path) {
	var params: HashMap<String, String>? = null

	override var buffer: ByteArray?
		get() = super.buffer
		set(buffer) {
			super.buffer = buffer
			parseParams()
		}

	init {
		parseParams()
	}

	open fun parseParams() {
		if (buffer === null) {
			return
		}
		params = HashMap()
		var param: List<String>
		for (paramPair in String(buffer!!).split(Pattern.quote("&").toRegex())) {
			if (paramPair.isEmpty()) {
				continue
			}
			param = paramPair.split(Pattern.quote("=").toRegex())
			if (param.size == 2) {
				try {
					params!![param[0]] = param[1]
				} catch (e: UnsupportedEncodingException) {
					e.printStackTrace()
				}
			}
		}
	}

	@Throws(IOException::class)
	override fun encode(sign: Sign?) {
		val builder = StringBuilder()
		params!!.forEach { (key, data) ->
			try {
				builder.append(key).append("=").append(data)
			} catch (e: UnsupportedEncodingException) {
				e.printStackTrace()
			}
		}
		this.buffer = builder.toString().toByteArray()
		super.encode(sign)
	}
}
