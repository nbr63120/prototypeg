/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.request

import com.gitlab.prototypeg.data.JsonSerializable
import com.gitlab.prototypeg.gfencryption.GfEncryptionInputStream
import com.gitlab.prototypeg.gfencryption.GfEncryptionOutputStream
import com.gitlab.prototypeg.gfencryption.Sign
import com.google.gson.JsonObject
import com.google.gson.JsonParser

import java.io.*
import java.net.URLDecoder
import java.net.URLEncoder
import java.util.Date

abstract class DataRequest(buffer: ByteArray, path: String) : ParamRequest(buffer, path), JsonSerializable<JsonObject> {
	open var date = Date()
	override var data: JsonObject = JsonObject()
		get() {
			if (!this.isDecoded) {
				this.decode(this.decodeSign)
			}
			return field
		}

	@Throws(IOException::class)
	override fun encode(sign: Sign?) {
		if (sign == null) {
			throw IOException(Sign::class.java.toString() + " is required to encode")
		}
		jsonSerialize()
		val byteArrayOutputStream = ByteArrayOutputStream()
		val outputStream = GfEncryptionOutputStream(byteArrayOutputStream)
		val outputStreamWriter = OutputStreamWriter(outputStream)
		outputStreamWriter.write(data.toString())
		outputStreamWriter.flush()
		outputStreamWriter.close()
		outputStream.finish(date, sign, false)
		outputStream.flush()
		outputStream.close()
		byteArrayOutputStream.flush()
		params!!["outdatacode"] = URLEncoder.encode(String(byteArrayOutputStream.toByteArray()), "UTF-8")
		byteArrayOutputStream.close()
		super.encode(sign)
	}

	@Throws(IOException::class)
	override fun decode(sign: Sign?) {
		if (sign == null) {
			throw IOException(Sign::class.java.toString() + " is required to decode")
		}
		if (!params!!.containsKey("outdatacode")) {
			throw IOException("not a DataRequest")
		}
		val byteArrayInputStream = ByteArrayInputStream(URLDecoder.decode(params!!["outdatacode"], "UTF-8").toByteArray())
		val inputStream = GfEncryptionInputStream(
				byteArrayInputStream,
				sign
		)
		val bufferedReader = BufferedReader(
				InputStreamReader(inputStream)
		)
		date = inputStream.date
		this.jsonDeserialize(bufferedReader)
		bufferedReader.close()
		inputStream.close()
		byteArrayInputStream.close()
		super.decode(sign)
	}

	override fun jsonSerialize() {}

	@Throws(IOException::class)
	protected open fun jsonDeserialize(reader: BufferedReader) {
		data = JsonParser.parseReader(reader) as JsonObject
	}
}
