/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.request

import com.gitlab.prototypeg.gfencryption.Sign

import java.io.IOException

open class UnknownParamRequest(buffer: ByteArray, path: String) : ParamRequest(buffer, path) {

	@Throws(IOException::class)
	override fun decode(sign: Sign?) {
		params!!.forEach { (key, data) ->
			print(key)
			print('=')
			println(data)
		}
		super.decode(sign)
	}
}
