/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network

import com.gitlab.prototypeg.gfencryption.Sign

import java.io.IOException

open class Decodable(open var buffer: ByteArray?) {

	open var decodeSign: Sign? = null

	open var isDecoded = false
		protected set

	open var isEdited = false

	@Throws(IOException::class)
	open fun encode(sign: Sign?) {
	}

	@Throws(IOException::class)
	open fun decode(sign: Sign?) {
		isDecoded = true
	}
}
