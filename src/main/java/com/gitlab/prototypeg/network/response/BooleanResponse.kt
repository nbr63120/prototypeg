/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.gfencryption.Sign
import com.gitlab.prototypeg.network.request.Request

open class BooleanResponse(request: Request, buffer: ByteArray) : Response(buffer, request) {

	open var value = false

	override fun encode(sign: Sign?) {
		buffer = byteArrayOf((if (value) '1' else '0').toByte())
	}

	override fun decode(sign: Sign?) {
		this.value = buffer!![0] != '0'.toByte()
	}
}
