/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.data.Skin
import com.gitlab.prototypeg.data.UserInfo
import com.gitlab.prototypeg.data.UserRecord
import com.gitlab.prototypeg.doll.Doll

import com.gitlab.prototypeg.network.request.Request
import com.google.gson.JsonObject

import java.util.LinkedHashMap

open class IndexResponse(buffer: ByteArray, request: Request) : JsonObjectResponse(buffer, request) {

	open var userInfo: UserInfo? = null
		get() {
			if (field === null) {
				field = get(USER_INFO, ::UserInfo)
			}
			return field
		}
		set(userInfo) {
			field = userInfo
			set(USER_INFO, userInfo)
		}
	open var userRecord: UserRecord? = null
		get() {
			if (field === null) {
				field = get(USER_RECORD, ::UserRecord)
			}
			return field
		}
		set(userRecord) {
			field = userRecord
			set(USER_RECORD, userRecord)
		}
	open var skinsWithUser: LinkedHashMap<Short, Skin>? = null
		get() {
			if (field === null) {
				val skinsWithUserInfo = data[SKINS_WITH_USER]?.asJsonObject
				if (skinsWithUserInfo !== null) {
					field = LinkedHashMap()
					skinsWithUserInfo.keySet().forEach {
						key -> field!![java.lang.Short.valueOf(key)] = Skin(skinsWithUserInfo[key].asJsonObject)
					}
				}
			}
			return field
		}

	open var dollsWithUser : ArrayList<Doll> = ArrayList() //TODO


	open var decensorFormula: String?
		get() = data[DECENSOR_FORMULA].asString
		set(decensorFormula) = data.addProperty(DECENSOR_FORMULA, decensorFormula)

	override fun jsonSerialize() {
		userRecord?.jsonSerialize()
		userInfo?.jsonSerialize()
		if (skinsWithUser != null) {
			val skinsWithUserInfo = JsonObject()
			skinsWithUser!!.forEach { (key, skin) -> skinsWithUserInfo.add(key.toString(), skin.data) }
			data.add(SKINS_WITH_USER, skinsWithUserInfo)
		}
	}

	companion object {
		const val USER_INFO = "user_info"
		const val USER_RECORD = "user_record"
		const val SKINS_WITH_USER = "skin_with_user_info"
		const val DECENSOR_FORMULA = "naive_build_gun_formula"

		const val DOLLS_WITH_USER = "gun_with_user_info"
	}
}
