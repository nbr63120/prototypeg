/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.data.DollWithUser
import com.gitlab.prototypeg.network.request.Request
import com.google.gson.JsonArray
import com.google.gson.JsonObject

import java.util.ArrayList

open class BattleFinishResponse(buffer: ByteArray, request: Request) : JsonObjectResponse(buffer, request) {

	open var rewardDolls: MutableList<DollWithUser>? = null
		get() {
			if (field == null) {
				val dolls = data.getAsJsonArray(REWARD_DOLLS)
				if (dolls != null) {
					rewardDolls = ArrayList()
					for (element in dolls) {
						field!!.add(DollWithUser(element as JsonObject))
					}
				}
			}
			return field
		}

	override fun jsonSerialize() {
		super.jsonSerialize()
		val dolls = JsonArray()
		for (doll in rewardDolls!!) {
			dolls.add(doll.data)
		}
		data.add(REWARD_DOLLS, dolls)
	}

	companion object {
		const val REWARD_DOLLS = "battle_get_gun"
	}
}
