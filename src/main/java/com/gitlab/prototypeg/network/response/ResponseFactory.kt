/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.network.request.Request

import java.util.HashMap

typealias ResponseCaster = (ByteArray, Request) -> Response

interface ResponseFactory {

	companion object {
		private val responseCasters = HashMap<String, ResponseCaster>()

		operator fun get(path: String, buffer: ByteArray, request: Request): Response {
			val responseCasters = responseCasters[path]
					?: return if (buffer[0] == '#'.toByte()) {
						UnknownJsonResponse(buffer, request)
					} else {
						UnknownResponse(buffer, request)
					}
			return responseCasters.invoke(buffer, request)
		}

		fun registerResponse(path: String, responseFactory: ResponseCaster) {
			responseCasters[path] = responseFactory
		}

		fun init() {
			val signResponseFactory: ResponseCaster = { buffer, request ->
				if (buffer[0] == '#'.toByte()) {
					SignResponse(buffer, request)
				} else {
					UnknownResponse(buffer, request)
				}
			}
			registerResponse("Index/getUidTianxiaQueue", signResponseFactory)
			registerResponse("Index/getDigitalSkyNbUid", signResponseFactory)
			registerResponse("Index/getUidEnMicaQueue", signResponseFactory)
			registerResponse("Index/index", ::IndexResponse)
			registerResponse("Gun/developGun", ::BuildDollResponse)
			registerResponse("Mission/battleFinish", ::BattleFinishResponse)
			registerResponse("Mission/endTurn", ::EndTurnResponse)
			registerResponse("Friend/visit", ::FriendVisitResponse)
		}
	}
}
