package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.network.request.Request
import com.google.gson.JsonArray

abstract class JsonArrayResponse(buffer: ByteArray, request: Request) : JsonResponse<JsonArray>(buffer, request) {
	override var data: JsonArray = JsonArray()
		get() {
			if (!this.isDecoded) {
				this.decode(this.decodeSign)
			}
			return field
		}
}