/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.network.request.Request
import com.gitlab.prototypeg.data.DollNumber

open class BuildDollResponse(buffer: ByteArray, request: Request) : JsonObjectResponse(buffer, request) {

	open var dollId: DollNumber?
		get() = data[DOLL_NUMBER].asShort
		set(dollId) = data.addProperty(DOLL_NUMBER, dollId.toString())

	companion object {
		const val DOLL_NUMBER = "gun_id"
	}
}
