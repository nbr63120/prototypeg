/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.network.request.Request
import com.google.gson.JsonElement
import com.google.gson.JsonNull

open class UnknownJsonResponse(buffer: ByteArray, request: Request) : JsonResponse<JsonElement>(buffer, request) {

	override var data: JsonElement = JsonNull()
		get() {
			if (!this.isDecoded) {
				this.decode(this.decodeSign)
			}
			return field
		}

	override fun jsonSerialize() {}
}
