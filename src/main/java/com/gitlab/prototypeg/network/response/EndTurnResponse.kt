/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network.response

import com.gitlab.prototypeg.data.MissionWinResult
import com.gitlab.prototypeg.network.request.Request
import com.google.gson.JsonObject

open class EndTurnResponse(buffer: ByteArray, request: Request) : JsonObjectResponse(buffer, request) {

	open var missionWinResult: MissionWinResult? = null
		get() {
			if (field === null) {
				missionWinResult = get(MISSION_WIN_RESULT, ::MissionWinResult)
			}
			return field
		}
		set(missionWinResult) {
			field = missionWinResult
			set(MISSION_WIN_RESULT, field)
		}

	companion object {
		const val MISSION_WIN_RESULT = "mission_win_result"
	}
}
