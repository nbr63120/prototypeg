/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.network

import com.gitlab.prototypeg.Session
import com.gitlab.prototypeg.gfencryption.Sign
import com.gitlab.prototypeg.network.request.Request
import com.gitlab.prototypeg.network.response.Response

class NetworkManager(val session: Session) {
	var sign = INITIAL_SIGN

	val requestHandlerManager: HandlerManager<Request> = HandlerManager(this, Request::class.java)
	val responseHandlerManager: HandlerManager<Response> = HandlerManager(this, Response::class.java)

	companion object {
		val INITIAL_SIGN = Sign("yundoudou")
	}
}
