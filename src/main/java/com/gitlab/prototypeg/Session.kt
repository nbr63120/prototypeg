/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg

import com.gitlab.prototypeg.data.UserInfo
import com.gitlab.prototypeg.doll.DollInfo
import com.gitlab.prototypeg.event.EventManager
import com.gitlab.prototypeg.event.IndexChangeEvent
import com.gitlab.prototypeg.network.NetworkManager
import com.gitlab.prototypeg.network.request.RequestFactory
import com.gitlab.prototypeg.network.response.IndexResponse
import com.gitlab.prototypeg.network.response.ResponseFactory

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.logging.Logger


open class Session {

	val executorService: ExecutorService = Executors.newFixedThreadPool(2)

	val eventManager = EventManager()

	val networkManager = NetworkManager(this)

	var uriHeader = "/index.php/1001/"

	var index: IndexResponse? = null
		set(index) {
			val indexChangeEvent = IndexChangeEvent(index)
			eventManager.call(indexChangeEvent)
			if (!indexChangeEvent.isCancelled) {
				field = indexChangeEvent.index
			}
		}
	init {
		networkManager.responseHandlerManager.registerHandler(MainResponseHandler(this))
	}

	companion object {
		var LOGGER: Logger = Logger.getLogger(Session::class.java.`package`.name)

		fun init() {
			RequestFactory.init()
			ResponseFactory.init()
			DollInfo.init()
		}
	}
}
