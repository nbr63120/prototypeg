package com.gitlab.prototypeg.doll

import com.gitlab.prototypeg.data.Language
import com.gitlab.prototypeg.data.DollNumber
import com.google.gson.JsonObject

open class UnknownDollInfo(override val dollNumber: DollNumber?) : DollInfo(JsonObject()) {
	override fun getName(language: Language): String {
		return "doll_number: $dollNumber"
	}

	override val rank: Byte?
		get() = 1
}