/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.doll

import com.gitlab.prototypeg.data.*
import com.google.gson.JsonObject

open class Doll(jsonObject: JsonObject) : JsonData(jsonObject){

	constructor(userId: UserId, dollId: DollId, dollNumber: DollNumber) : this(JsonObject()){
		this.dollId = dollId
		this.userId = userId
		this.dollNumber = dollNumber
		experience = 0
		level = 1
		teamNumber = 0
		modification = 0
		teamIndex = 0
		teamPosition = 0
		life = 0 //TODO calc
		ammo = 0 //TODO *
		mre = 0 //TODO *
		power = 0
		hit = 0
		dodge = 0
		fireRate = 0
		skill1 = 1
		skill2 = 0
		fixEndTime = 0
		isLocked = false
		number = 0 //TODO unknown
		equip1 = 0
		equip2 = 0
		equip3 = 0
		equip4 = 0
		favor = 500000
		maxFavor = 500000
		favorLimit = 1000000
		isSoulBond = false
		skinNumber = 0
		isClickable = false
		soulBondTime = 0
		specialEffect = 0
	}

	open var dollId : DollId?
		get() = data[DOLL_ID].asInt
		set(dollId) = data.addProperty(DOLL_ID, dollId.toString())

	open var userId : UserId?
		get() = data[USER_ID].asInt
		set(userId) = data.addProperty(USER_ID, userId.toString())

	open var dollNumber : DollNumber?
		get() = data[DOLL_NUMBER].asShort
		set(dollNumber) = data.addProperty(DOLL_NUMBER, dollNumber.toString())

	open var experience : Int?
		get() = data[EXPERIENCE].asInt
		set(experience) = data.addProperty(EXPERIENCE, experience.toString())

	open var level : Byte?
		get() = data[LEVEL].asByte
		set(level) = data.addProperty(LEVEL, level.toString())

	open var teamNumber : Byte?
		get() = data[TEAM_NUMBER].asByte
		set(teamNumber) = data.addProperty(TEAM_NUMBER, teamNumber.toString())

	open var modification : Byte?
		get() = data[MODIFICATION].asByte
		set(modification) = data.addProperty(MODIFICATION, modification.toString())

	open var teamIndex : Byte?
		get() = data[TEAM_INDEX].asByte
		set(teamIndex) = data.addProperty(TEAM_INDEX, teamIndex.toString())

	open var teamPosition : Byte?
		get() = data[TEAM_POSITION].asByte
		set(teamIndex) = data.addProperty(TEAM_POSITION, teamIndex.toString())

	open var life : Short?
		get() = data[LIFE].asShort
		set(life) = data.addProperty(LIFE, life.toString())

	open var ammo : Byte?
		get() = data[AMMO].asByte
		set(ammo) = data.addProperty(AMMO, ammo.toString())

	open var mre : Byte?
		get() = data[MRE].asByte
		set(mre) = data.addProperty(MRE, mre.toString())

	open var power : Short?
		get() = data[POWER].asShort
		set(value) = data.addProperty(POWER, value.toString())

	open var hit : Short?
		get() = data[HIT].asShort
		set(hit) = data.addProperty(HIT, hit.toString())

	open var dodge : Short?
		get() = data[DODGE].asShort
		set(value) = data.addProperty(DODGE, value.toString())

	open var fireRate : Short?
		get() = data[FIRE_RATE].asShort
		set(value) = data.addProperty(FIRE_RATE, value.toString())

	open var skill1 : Byte?
		get() = data[SKILL1].asByte
		set(value) = data.addProperty(SKILL1, value.toString())

	open var skill2 : Byte?
		get() = data[SKILL2].asByte
		set(value) = data.addProperty(SKILL2, value.toString())

	open var fixEndTime : Long?
		get() = data[FIX_END_TIME].asLong
		set(value) = data.addProperty(FIX_END_TIME, value.toString())

	open var isLocked : Boolean?
		get() = data[IS_LOCKED].asString != "0"
		set(value) = data.addProperty(IS_LOCKED,
			when(value) {
				null -> null
				true -> "1"
				else -> "0"
			}
		)

	open var number : Byte?
		get() = data[NUMBER].asByte
		set(value) = data.addProperty(NUMBER, value.toString())

	open var equip1 : Int?
		get() = data[EQUIP1].asInt
		set(value) = data.addProperty(EQUIP1, value.toString())

	open var equip2 : Int?
		get() = data[EQUIP2].asInt
		set(value) = data.addProperty(EQUIP2, value.toString())

	open var equip3 : Int?
		get() = data[EQUIP3].asInt
		set(value) = data.addProperty(EQUIP3, value.toString())

	open var equip4 : Int?
		get() = data[EQUIP4].asInt
		set(value) = data.addProperty(EQUIP4, value.toString())

	open var favor : Int?
		get() = data[FAVOR].asInt
		set(value) = data.addProperty(FAVOR, value.toString())

	open var maxFavor : Int?
		get() = data[MAX_FAVOR].asInt
		set(value) = data.addProperty(MAX_FAVOR, value.toString())

	open var favorLimit : Int?
		get() = data[FAVOR_LIMIT].asInt
		set(value) = data.addProperty(FAVOR_LIMIT, value.toString())

	open var isSoulBond : Boolean?
		get() = data[IS_SOUL_BOND].asString != "0"
		set(value) = data.addProperty(IS_SOUL_BOND,
				when(value) {
					null -> null
					true -> "1"
					else -> "0"
				}
		)

	open var skinNumber : SkinNumber?
		get() = data[SKIN_NUMBER].asShort
		set(value) = data.addProperty(SKIN_NUMBER, value.toString())

	open var isClickable : Boolean?
		get() = data[IS_ClICKABLE].asString != "0"
		set(value) = data.addProperty(IS_ClICKABLE,
				when(value) {
					null -> null
					true -> "1"
					else -> "0"
				}
		)

	open var soulBondTime : Long?
		get() = data[SOUL_BOND_TIME].asLong
		set(value) = data.addProperty(SOUL_BOND_TIME, value.toString())

	open var specialEffect : Int? //TODO check type
		get() = data[SPECIAL_EFFECT].asInt
		set(value) = data.addProperty(SPECIAL_EFFECT, value.toString())

	open val info : DollInfo
		get() = DollInfo[dollNumber]

	companion object {
		const val DOLL_ID = "id"
		const val USER_ID = "user_id"
		const val DOLL_NUMBER = "gun_id"
		const val EXPERIENCE = "gun_exp"
		const val LEVEL = "gun_level"
		const val TEAM_NUMBER = "team_id"
		const val MODIFICATION = "if_modification"
		const val TEAM_INDEX = "location"
		const val TEAM_POSITION = "position"
		const val LIFE = "life"
		const val AMMO = "ammo"
		const val MRE = "mre"
		const val POWER = "pow"
		const val HIT = "hit"
		const val DODGE = "dodge"
		const val FIRE_RATE = "rate"
		const val SKILL1 = "skill1"
		const val SKILL2 = "skill2"
		const val FIX_END_TIME = "fix_end_time"
		const val IS_LOCKED = "is_locked"
		const val NUMBER = "number"
		const val EQUIP1 = "equip1"
		const val EQUIP2 = "equip2"
		const val EQUIP3 = "equip3"
		const val EQUIP4 = "equip4"
		const val FAVOR = "favor"
		const val MAX_FAVOR = "max_favor"
		const val FAVOR_LIMIT = "favor_toplimit"
		const val IS_SOUL_BOND = "soul_bond" //expected as auth
		const val SKIN_NUMBER = "skin"
		const val IS_ClICKABLE = "can_click"
		const val SOUL_BOND_TIME = "soul_bond_time"
		const val SPECIAL_EFFECT = "special_effect"
	}
}
