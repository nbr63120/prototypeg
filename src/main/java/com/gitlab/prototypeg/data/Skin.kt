/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.data

import com.google.gson.JsonObject

open class Skin(jsonObject: JsonObject?) : JsonData(jsonObject) {

	open var skinNumber: SkinNumber?
		get() = data.get(SKIN_NUMBER)?.asString?.toShort()
		set(skinNumber) = data.addProperty(SKIN_NUMBER, skinNumber.toString())

	open var userId: UserId?
		get() = data.get(USER_ID)?.asString?.toInt()
		set(userId) = data.addProperty(USER_ID, userId)

	open var isRead: Boolean
		get() = data.get(IS_READ)?.asString != "0"
		set(read) = data.addProperty(IS_READ, if (read) "0" else "1")

	companion object {
		const val SKIN_NUMBER = "skin_id"
		const val USER_ID = "user_id"
		const val IS_READ = "is_read"
	}
}
