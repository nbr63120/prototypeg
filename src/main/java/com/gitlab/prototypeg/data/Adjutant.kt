/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.data

open class Adjutant(var dollNumber: DollNumber, var skinNumber: Short, var isDamaged: Boolean, var isModded: Boolean) {
	override fun toString(): String {
		return arrayOf(
				this.dollNumber.toString(),
				this.skinNumber.toString(),
				(if (this.isDamaged) 1 else 0).toString(),
				(if (this.isModded) 1 else 0).toString()).joinToString(
				",")
	}
}
