/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 * TODO: add unknown fields
 */

package com.gitlab.prototypeg.data

import com.google.gson.JsonObject

import java.util.ArrayList
import java.util.StringJoiner

open class UserInfo(data: JsonObject) : JsonData(data) {

	open var gems: Int?
		get() = data[GEMS]?.asInt
		set(gems) = data.addProperty(GEMS, gems.toString())

	open var id: Int?
		get() = data[ID]?.asInt
		set(id) = data.addProperty(ID, id.toString())

	open var userId: UserId?
		get() = data[USER_ID]?.asInt
		set(userId) = data.addProperty(USER_ID, userId.toString())

	open var openId: Int?
		get() = data.get(OPEN_ID)?.asInt
		set(openId) = data.addProperty(OPEN_ID, openId.toString())

	open var channelId: Int?
		get() = data[CHANNEL_ID]?.asInt
		set(channelId) = data.addProperty(CHANNEL_ID, channelId.toString())

	open var sign: String?
		get() = data[SIGN]?.asString
		set(sign) = data.addProperty(SIGN, sign.toString())

	open var name: String?
		get() = data[NAME]?.asString
		set(name) = data.addProperty(NAME, name.toString())

	open var experience: Int?
		get() = data[EXPERIENCE]?.asInt
		set(experience) = data.addProperty(EXPERIENCE, experience.toString())

	open var level: Int?
		get() = data[LEVEL]?.asInt
		set(level) = data.addProperty(LEVEL, level.toString())

	open var maxEquipments: Int?
		get() = data[MAX_EQUIPMENTS]?.asInt
		set(maxEquipments) = data.addProperty(MAX_EQUIPMENTS, maxEquipments.toString())

	open var maxDolls: Int?
		get() = data[MAX_DOLLS]?.asInt
		set(maxDolls) = data.addProperty(MAX_DOLLS, maxDolls.toString())

	open var maxTeams: Int?
		get() = data[MAX_TEAMS]?.asInt
		set(maxTeams) = data.addProperty(MAX_TEAMS, maxTeams.toString())

	open var maxBuildSlots: Int?
		get() = data[MAX_BUILD_SLOTS]?.asInt
		set(maxBuildSlots) = data.addProperty(MAX_BUILD_SLOTS, maxBuildSlots.toString())

	open var maxEquipmentBuildSlots: Int?
		get() = data[MAX_EQUIPMENT_BUILD_SLOTS]?.asInt
		set(maxEquipmentBuildSlots) = data.addProperty(MAX_EQUIPMENT_BUILD_SLOTS, maxEquipmentBuildSlots.toString())

	open var maxFixSlots: Int?
		get() = data[MAX_FIX_SLOTS]?.asInt
		set(maxFixSlots) = data.addProperty(MAX_FIX_SLOTS, maxFixSlots.toString())

	open var maxUpgradeSlots: Int?
		get() = data[MAX_UPGRADE_SLOTS]?.asInt
		set(maxUpgradeSlots) = data.addProperty(MAX_UPGRADE_SLOTS, maxUpgradeSlots.toString())

	open var maxTeamPresets: Int?
		get() = data[MAX_TEAM_PRESETS]?.asInt
		set(maxTeamPresets) = data.addProperty(MAX_TEAM_PRESETS, maxTeamPresets.toString())

	open var maxFairies: Int?
		get() = data[MAX_FAIRIES]?.asInt
		set(maxFairies) = data.addProperty(MAX_FAIRIES, maxFairies.toString())

	open var battlePoints: Int?
		get() = data[BATTLE_POINTS]?.asInt
		set(battlePoints) = data.addProperty(BATTLE_POINTS, battlePoints.toString())

	open var extraBattlePoints: Int?
		get() = data[EXTRA_BATTLE_POINTS]?.asInt
		set(extraBattlePoints) = data.addProperty(EXTRA_BATTLE_POINTS, extraBattlePoints.toString())

	open var manPower: Int?
		get() = data[MAN_POWER]?.asInt
		set(manPower) = data.addProperty(MAN_POWER, manPower.toString())

	open var ammo: Int?
		get() = data[AMMO]?.asInt
		set(ammo) = data.addProperty(AMMO, ammo.toString())

	open var mre: Int?
		get() = data[MRE]?.asInt
		set(mre) = data.addProperty(MRE, mre.toString())

	open var parts: Int?
		get() = data[PARTS]?.asInt
		set(parts) = data.addProperty(PARTS, parts.toString())

	open var cores: Int?
		get() = data[CORES]?.asInt
		set(cores) = data.addProperty(CORES, cores.toString())

	open var basicData: Int?
		get() = data[BASIC_DATA]?.asInt
		set(basicData) = data.addProperty(BASIC_DATA, basicData.toString())

	open var intermediateData: Int?
		get() = data[INTERMEDIATE_DATA]?.asInt
		set(intermediateData) = data.addProperty(INTERMEDIATE_DATA, intermediateData.toString())

	open var advancedData: Int?
		get() = data[ADVANCED_DATA]?.asInt
		set(advancedData) = this.data.addProperty(ADVANCED_DATA, advancedData.toString())

	open var lastRecoverTime: Long?
		get() = data[LAST_RECOVER_TIME]?.asLong
		set(lastRecoverTime) = data.addProperty(LAST_RECOVER_TIME, lastRecoverTime.toString())

	open var lastBattlePointRecoverTime: Long?
		get() = data[LAST_BATTLE_POINT_RECOVER_TIME]?.asLong
		set(lastBattlePointRecoverTime) = data.addProperty(LAST_BATTLE_POINT_RECOVER_TIME, lastBattlePointRecoverTime.toString())

	open var lastFavorRecoverTime: Long?
		get() = data[LAST_FAVOR_RECOVER_TIME]?.asLong
		set(lastFavorRecoverTime) = data.addProperty(LAST_FAVOR_RECOVER_TIME, lastFavorRecoverTime.toString())

	open var lastLoginTime: Long?
		get() = data[LAST_LOGIN_TIME]?.asLong
		set(lastLoginTime) = data.addProperty(LAST_LOGIN_TIME, lastLoginTime.toString())

	open var collectedDolls: ArrayList<DollNumber>? = null
		get() {
			if (field == null) {
				val dolls = this.data[COLLECTED_DOLLS]?.asString?.split(",".toRegex())
				if (dolls != null) {
					collectedDolls = ArrayList()
					for (doll in dolls) {
						field!!.add(java.lang.Short.parseShort(doll))
					}
				}
			}
			return field
		}

	open var maxDormitories: Int?
		get() = data[MAX_DORMITORIES]?.asInt
		set(maxDormitories) = data.addProperty(MAX_DORMITORIES, maxDormitories)

	override fun jsonSerialize() {
		if (collectedDolls != null) {
			val dolls = StringJoiner(",")
			for (dollNumber in collectedDolls!!) {
				dolls.add(dollNumber.toString())
			}
			data.addProperty(COLLECTED_DOLLS, dolls.toString())
		}
	}

	companion object {
		const val GEMS = "gem"
		const val ID = "id"
		const val USER_ID = "user_id"
		const val OPEN_ID = "open_id"
		const val CHANNEL_ID = "channel_id"
		const val SIGN = "sign"
		const val NAME = "name"
		//const static final String GEM_PAY = "gem_pay";
		//"pause_turn_chance";
		const val EXPERIENCE = "experience"
		const val LEVEL = "lv"
		const val MAX_EQUIPMENTS = "maxequip"
		const val MAX_DOLLS = "maxgun"
		const val MAX_TEAMS = "maxteam"
		const val MAX_BUILD_SLOTS = "max_build_slot"
		const val MAX_EQUIPMENT_BUILD_SLOTS = "max_equip_build_slot"
		const val MAX_FIX_SLOTS = "max_fix_slot"
		const val MAX_UPGRADE_SLOTS = "max_upgrade_slot"
		const val MAX_TEAM_PRESETS = "max_gun_preset"
		const val MAX_FAIRIES = "max_fairy"
		const val BATTLE_POINTS = "bp"
		const val EXTRA_BATTLE_POINTS = "bp_pay"
		const val MAN_POWER = "mp"
		const val AMMO = "ammo"
		const val MRE = "mre"
		const val PARTS = "part"
		const val CORES = "core"
		const val BASIC_DATA = "coin1"
		const val INTERMEDIATE_DATA = "coin2"
		const val ADVANCED_DATA = "coin3"
		//"monthlycard1_end_time";
		//"monthlycard2_end_time";
		//"monthlycard3_end_time";
		//"growthfond";
		const val LAST_RECOVER_TIME = "last_recover_time"
		const val LAST_BATTLE_POINT_RECOVER_TIME = "last_bp_recover_time"
		const val LAST_FAVOR_RECOVER_TIME = "last_favor_recover_time"
		//"last_monthlycard1_recover_time";
		//"last_monthlycard2_recover_time";
		const val LAST_LOGIN_TIME = "last_login_time"
		//"reg_time";
		const val COLLECTED_DOLLS = "gun_collect"
		const val MAX_DORMITORIES = "maxdorm"
		//"maxdorm_spare";
		//"last_dorm_material_change_time1";
		//"last_dorm_material_change_time2";
		//"material_available_num1";
		//"material_available_num2";
		//"dorm_max_score";
		//"last_ssoc_change_time";
		//"app_guard_id";
		//"app_guard_num";
		//"is_bind";
		//"dorm_max_score_lifetime";
		const val GENDER = "gender"
	}
}
