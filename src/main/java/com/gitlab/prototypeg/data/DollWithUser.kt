/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.data

import com.google.gson.JsonObject

open class DollWithUser(data: JsonObject) : JsonData(data) {

	open var dollId: DollId?
		get() = data.get(DOLL_ID)?.asInt
		set(dollId) = data.addProperty(DOLL_ID, dollId.toString())

	open var dollNumber: DollNumber?
		get() = data[DOLL_NUMBER].asShort
		set(id) = data.addProperty(DOLL_NUMBER, id.toString())

	companion object {
		const val DOLL_ID = "gun_with_user_id"
		const val DOLL_NUMBER = "gun_id"
	}
}
