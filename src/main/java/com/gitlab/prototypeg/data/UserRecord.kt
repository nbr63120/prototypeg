/*
 *     ____             __        __                   ______
 *    / __ \_________  / /_____  / /___  ______  ___  / ____/
 *   / /_/ / ___/ __ \/ __/ __ \/ __/ / / / __ \/ _ \/ / __
 *  / ____/ /  / /_/ / /_/ /_/ / /_/ /_/ / /_/ /  __/ /_/ /
 * /_/   /_/   \____/\__/\____/\__/\__, / .___/\___/\____/
 *                                /____/_/
 * @author djdisodo
 * @link https://gitlab.com/djdisodo/prototypeg
 *
 */

package com.gitlab.prototypeg.data

import com.google.gson.JsonObject

import java.util.Date

open class UserRecord(data: JsonObject) : JsonData(data) {

	open var adjutant: Adjutant? = null
		get() {
			if (field === null) {
				val adjutantData = this.data[ADJUTANT]?.asString?.split(",".toRegex())
				if (adjutantData !== null) {
					field = Adjutant(
							adjutantData[0].toShort(),
							adjutantData[1].toShort(),
							adjutantData[2].toInt() != 0,
							adjutantData[2].toInt() != 0
					)
				}
			}
			return field
		}

	open var userId: Int?
		get() = data[USER_ID]?.asInt
		set(userId) = data.addProperty(USER_ID, userId.toString())

	open var missionCampaign: Byte?
		get() = this.data[MISSION_CAMPAIGN]?.asByte
		set(missionCampaign) = data.addProperty(MISSION_CAMPAIGN, missionCampaign.toString())

	open var specialMissionCampaign: Byte?
		get() = data[SPECIAL_MISSION_CAMPAIGN]?.asByte
		set(specialMissionCampaign) = data.addProperty(SPECIAL_MISSION_CAMPAIGN, specialMissionCampaign.toString())

	open var lastBuildDollTime: Long?
		get() = data[LAST_BUILD_DOLL_TIME]?.asLong
		set(lastBuildDollTime) = this.data.addProperty(LAST_BUILD_DOLL_TIME, lastBuildDollTime.toString())

	open var nextCoreDollRecoverTime: Long?
		get() = data[NEXT_CORE_RECOVER_DOLL_TIME]?.asLong
		set(nextCoreDollRecoverTime) = data.addProperty(LAST_BUILD_DOLL_TIME, lastBuildDollTime.toString())

	override fun jsonSerialize() {
		if (adjutant != null) {
			this.data.addProperty(ADJUTANT, this.adjutant!!.toString())
		}
	}

	companion object {
		//"last_datacell_change_time": "1569593554",
		//"buymp_num": "4963",
		//"buyammo_num": "67",
		//"buymre_num": "65",
		//"buypart_num": "67",
		//"develop_nonew_num": "71479"
		const val USER_ID = "user_id"
		const val MISSION_CAMPAIGN = "mission_campaign"
		const val SPECIAL_MISSION_CAMPAIGN = "special_mission_campaign"
		//"attendance_type1_day"
		//"attendance_type1_time": 1579359600,
		//"attendance_type2_day": "1",
		//"attendance_type2_time": "1570460400",
		//"develop_lowrank_count": 4,
		//"special_develop_lowrank_count": "0",
		//"get_gift_ids": "",
		//"spend_point": "0.00",
		//"gem_mall_ids": "",
		//"product_type21_ids": "",
		//"seven_type": "2",
		//"seven_start_time": "1569510000",
		//"seven_attendance_days": "1,2,3,4,5,6,7",
		//"seven_spend_point": "0",
		//"last_bp_buy_time": "0",
		//"bp_buy_count": "0",
		//"new_developgun_count": "10",
		//"buyitem1_developgun_count": 0.3,
		//"buyitem1_specialdevelopgun_count": "50.00",
		//"buyitem1_num": "0",
		const val LAST_BUILD_DOLL_TIME = "last_developgun_time"
		const val LAST_SPECIAL_BUILD_DOLL_TIME = "last_specialdevelopgun_time"
		//"furniture_classes": "2",
		const val ADJUTANT = "adjutant"
		//"adjutant_fairy": "",
		//"mission_group_today_reset_num": 0,
		//"mission_group_last_reset_time": "0",
		//"spendpoint_age_id": "0",
		//"spend_point_thismonth": "0",
		//"last_spendpoint_time": "0",
		const val NEXT_CORE_RECOVER_DOLL_TIME = "next_core_recover_gun_time"
	}
}
